# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.2.0] - 2019-03-21

### New features

- Upgrade to the latest current TypeScript version (3.3.4000)

## [3.1.1] - 2019-02-01

### Bugfixes

- No longer throw an error when `paths` is not defined in `tsconfig.json`. (Thanks @yannik.wissner!)

## [3.1.0] - 2019-01-18

### New features

- When you define `paths` in your `tsconfig.json`, those will now also be resolved.

## [3.0.2] - 2019-01-18

### Bugfixes

- The peer dependency on React Static now points to an actually existing version (thanks @yanneves!)

## [3.0.1] - 2018-12-31

### Bugfixes

- Modifications to the list of Webpack loaders (e.g. made by other plug-ins) are no longer erased. In other words, you now no longer need to add this plugin _before_ other loader-modifying plugins. Fixes [#3](https://gitlab.com/Vinnl/react-static-plugin-typescript/issues/3).

## [3.0.0] - 2018-12-31

### Breaking changes

- Type checking is now enabled by default. Set [`typeCheck`](https://www.npmjs.com/package/react-static-plugin-typescript#typecheck-boolean) to `false` to return to the previous default.

## [2.1.0] - 2018-12-31

### New features

- Type checking can now be enabled by adding the [option](https://github.com/nozzle/react-static/tree/master/docs/plugins#plugin-options) `typeCheck: true`.

## [2.0.0] - 2018-12-24

### New features

- Moved from [ts-loader](https://github.com/TypeStrong/ts-loader/) to regular [Babel](https://blogs.msdn.microsoft.com/typescript/2018/08/27/typescript-and-babel-7/). This builds on React Static's own Babel config, making sure to remain compatible with e.g. transformers added by React Static. Furthermore, this is the setup also used by Create React App, making sure we're in line with the expectations of the wider React community.

### Breaking changes

- The move to Babel should not impact most code bases. However, this does mean we are now limited by all [caveats that apply to Babel's TypeScript support](https://babeljs.io/docs/en/babel-plugin-transform-typescript#caveats). Luckily, those caveats [should not strongly affect React developers](https://github.com/facebook/create-react-app/pull/4837#issuecomment-430107471).

## [1.0.1] - 2018-12-19

### Bugfixes

- Use correct package name in README.

## [1.0.0] - 2018-12-19

### New features

- First release!
